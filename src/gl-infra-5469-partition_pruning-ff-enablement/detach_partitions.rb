# frozen_string_literal: true

DRY_RUN = true # Change to false to actually detach old partitions

EXPECTED_NAMES_TO_DETACH = %w[
  web_hook_logs_000000
  web_hook_logs_202007
  web_hook_logs_202008
  web_hook_logs_202009
  web_hook_logs_202010
  web_hook_logs_202011
  web_hook_logs_202012
  web_hook_logs_202101
  web_hook_logs_202102
  web_hook_logs_202103
  web_hook_logs_202104
  web_hook_logs_202105
].to_set

EXPECTED_NAMES_TO_REMAIN = %w[
  web_hook_logs_202106
  web_hook_logs_202107
  web_hook_logs_202108
  web_hook_logs_202109
  web_hook_logs_202110
  web_hook_logs_202111
  web_hook_logs_202112
  web_hook_logs_202201
  web_hook_logs_202202
  web_hook_logs_202203
].to_set

def pretty_print_set(s)
  if s.empty?
    '<None>'
  else
    s.to_a.sort.join("\n")
  end
end

def assert_detaching_correct_partitions!
  names_to_detach = WebHookLog.partitioning_strategy.extra_partitions.map(&:partition_name).to_set
  if names_to_detach.empty?
    puts <<~MSG
      [Warning] None of the extra web_hook_logs partitions are present. Have they already been detached?
      This is expected in two possible scenarios:
       - This script has already run successfully
       - After enabling the partition_pruning feature flag, the scheduled job ran (either every 6 hours or as part of rails startup)
          and detached the old partitions already.
    MSG
    return # Safe to skip the check - we're not detaching anything, and will proceed to verifying state after detach
  end

  unless names_to_detach == EXPECTED_NAMES_TO_DETACH
    raise <<~MSG
      Would detach the wrong partitions!
      Partitions we expect to detach but would not:
      #{pretty_print_set(EXPECTED_NAMES_TO_DETACH - names_to_detach)}
      Partitions we would detach but expect not to:
      #{pretty_print_set(names_to_detach - EXPECTED_NAMES_TO_DETACH)}
    MSG
  end

  puts 'Verified the list of partitions to detach'
end

def verify_correct_partitions_exist_after_detach!
  ApplicationRecord.transaction do # So that we read from a primary, rather than a replica
    current_partitions = Gitlab::Database::PostgresPartition.for_parent_table('web_hook_logs').pluck(:name).to_set
    unless current_partitions == EXPECTED_NAMES_TO_REMAIN
      raise <<~MSG
        Incorrect partitions were detached!
        Partitions that we expect to exist, but were detached:
        #{pretty_print_set(EXPECTED_NAMES_TO_REMAIN - current_partitions)}
        Partitions we expect to have detached, but did not:
        #{pretty_print_set(current_partitions - EXPECTED_NAMES_TO_REMAIN)}
      MSG
    end
  end
end

def run_detach!
  return if DRY_RUN

  Gitlab::Database::Partitioning.sync_partitions
end

def run
  puts 'Verifying partitions that will be detached'

  assert_detaching_correct_partitions!

  if DRY_RUN
    puts 'Dry run - no partitions have been detached'
  else
    puts 'Detaching old partitions'
    run_detach!
    puts 'Partitions detached'
    verify_correct_partitions_exist_after_detach!
    puts 'SUCCESS - verified the correct partitions are present, and the correct ones were detached'
  end
end

run
